var errorName = document.getElementById("error-name");
var errorEmail = document.getElementById("error-email");
var errorContent = document.getElementById("error-content");


var isValidEmail = false;
var isValidName = false;
var isValidContent = false;

/*
    Limpiamos el contendor del mensaje de error
*/
resetTextContainer= ( field ) => {

    setTimeout(() => {
        field.innerHTML = '';
    }, 3000);

}

/*
    La validación del HTML generalmente no valida en su totalidad si es un dirección de
    correo electrónico valido, por lo se define una expresión regular que nos ayude 
    a corroborar la validéz de la dirección.
*/
emailValidation = () => {

    const regExp = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    const isValid = regExp.test( email.value );
    
    if( isValid ) {
        isValidEmail = true;
        return true;
        errorEmail.innerHTML = '';
    } else {
        errorEmail.innerHTML = 'El correo no es válido';
        resetTextContainer( errorEmail );
        return null
    }
}

/*
    Método para envíar un simple correo de confirmación al remitente
*/

sendEmail = ( name, email ) => {

    Email.send({

        Host: 'smtp.elasticemail.com',
        Username: 'bryan_riosb@outlook.com',
        Password: '761FB2E2EDD30A2AC7220BC6F44B16D9E6F5',
        From: 'bryan_riosb@outlook.com',
        To: email,
        Subject: 'Mensaje recibido por BORLS',
        Body: `Estimado ${ name }. Hemos recibido con éxito su mensanje en un termino no mayor a 24 horas le daremos respuesta a su requerimiento`

    }).then((event)=>{

        // console.log("Enviado ", event);
        const form = document.getElementById("contact-form");
        const msgSent = document.getElementById("msg-sent");

        msgSent.innerHTML = 'Su mesaje se ha enviado correctamete en breve nos comunicaremos con usted';
        form.reset();
        resetTextContainer( msgSent );

    }).catch(e => {
        console.log('Hay problema con el servidor de correo debido a : ', e);
    });
        
}


sendMessage = () => {

    const name = document.getElementById("name");
    const email = document.getElementById("email");
    const content = document.getElementById("content");

    /*
        Condicionales para definir un mensaje de error personalizado
    */
    try {

        if( name.value === null || name.value === '' ) {
            errorName.innerHTML = 'Por favor ingrese su nombre';
            resetTextContainer( errorName );
        } else {
            isValidName = true;
        }
     
        if( email.value === null || email.value === '' ) {
            errorEmail.innerHTML = 'El correo electrónico es requerido';
            resetTextContainer( errorEmail );
        } else {
            emailValidation();
        }
     
        if( content.value == null || content.value == '' ) {
            errorContent.innerHTML = 'No haz escrito un mensaje';
            resetTextContainer( errorContent );
        } else {
            isValidContent = true;
        }

        if( isValidName == true && isValidEmail == true && isValidContent == true) {

            console.log('Email: ', email.value);
            
            sendEmail( name.value, email.value );
        }
     
    } catch(e) {

        alert('Opps! Hubo problemas al intentar enviar el correo');
        console.log(e);

    }
    
}



