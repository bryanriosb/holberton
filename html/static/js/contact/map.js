
mapboxgl.accessToken = 'pk.eyJ1IjoiYm9yZXByb2plY3QiLCJhIjoiY2pwbzdteDNoMDhlMjRhbjdjazZ5Mm5uOCJ9.HnZ1Z66PmeXSbA3vsQWt3g';

const point = [-76.5088237, 3.4622817];

const map = new mapboxgl.Map({

    style: 'mapbox://styles/boreproject/cjsjdesuc28kn1flevgabrb3u',
    center: point,
    zoom: 16,
    pitch: 45,
    bearing: -17.6,
    container: 'map',
    antialias: true

});

// Creación del popup
const popup = new mapboxgl.Popup({ offset: 25 }).setText(
    'Oficina central - Calle 43 # 3a -23'
    );
     
// creación del DOM element para el marcador
const markerItem = document.createElement( 'div' );
markerItem.id = 'marker';


// Creación del marcador
const marker = new mapboxgl.Marker( markerItem )
    .setLngLat ( point )
    .setPopup( popup )
    .addTo( map );

/*
    la capa de edificios en la fuente del vector mapbox streets contiene la altura de algunos edificios
    desde la base de datos de OpenStreetMap.
*/

map.on( 'load', () => {
    
    // Se inserta debajo la capa de estilo
    const layers = map.getStyle().layers;
     
    var labelLayerId;

    for (let i = 0; i < layers.length; i++) {

        if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
            labelLayerId = layers[i].id;
            break;
        }
    }
     
    map.addLayer(
        {
            'id': '3d-buildings',
            'source': 'composite',
            'source-layer': 'building',
            'filter': ['==', 'extrude', 'true'],
            'type': 'fill-extrusion',
            'minzoom': 15,
            'paint': {
                'fill-extrusion-color': '#aaa',
                /*
                    Se usa la expresion 'interpolate' para agregar un efecto de extrusión según altura
                    al hacer zoom
                */
                'fill-extrusion-height': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    15,
                    0,
                    15.05,
                    ['get', 'height']
                ],
                'fill-extrusion-base': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    15,
                    0,
                    15.05,
                    ['get', 'min_height']
                ],
                'fill-extrusion-opacity': 0.8
            }
        },
        labelLayerId
    );
});